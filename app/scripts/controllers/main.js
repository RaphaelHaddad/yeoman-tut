'use strict';

/**
 * @ngdoc function
 * @name yeomanTutApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yeomanTutApp
 */
angular.module('yeomanTutApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
